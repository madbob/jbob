/*
 * jBob - Opinionated utilities for jQuery and Bootstrap
 * Copyright (C) 2021/2025  Roberto Guido
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class jBob {
	init(options) {
		let defaults = {
			beforeInitFunction: function() {},
			initFunction: function() {},
			dynamicFunctions: {},
			fixBootstrap: [],
		};

		this.mainOptions = $.extend({}, defaults, options || {});
		this.fixBootstrap(['Modal', 'Popover'].concat(this.mainOptions.fixBootstrap));

		this.onceInit();
		this.initElements($('body'));
	}

	/*
		This function has been get directly from Bootstrap, and is intended to
		init related jQuery plugins. Within certain environments (e.g Vite
		build) this is not properly called on load (due jQuery is not updated to
		the latest non-sense JS "best pratices" defined by people having nothing
		to do all day long, and enjoying to continuosly break stuffs), so we
		enforce the initialization here.
		With no mercy, nor elegance,  but to get things done.
	*/
	defineJQueryPlugin(plugin) {
		const name = plugin.NAME;
		if (name in $.fn == false) {
			const JQUERY_NO_CONFLICT = $.fn[name];
			$.fn[name] = plugin.jQueryInterface;
			$.fn[name].Constructor = plugin;
			$.fn[name].noConflict = () => {
				$.fn[name] = JQUERY_NO_CONFLICT;
				return plugin.jQueryInterface;
			}
		}
	}

	fixBootstrap(plugins) {
		if (window.bootstrap) {
			plugins.forEach((plug) => {
				this.defineJQueryPlugin(window.bootstrap[plug]);
			});
		}
	}

	assignIDs(selectors, container) {
		selectors.forEach(function(selector, index) {
			$(selector, container).not('[id]').each(function() {
				$(this).attr('id', Math.random().toString(36).substring(2));
			});
		});
	}

	makeSpinner() {
		return '<div class="text-center"><div class="spinner-border" role="status"></div></div>';
	}

	formValidation(form) {
		if (form.hasClass('needs-validation')) {
			form.addClass('was-validated');
			return form.get(0).checkValidity();
		} else {
			return true;
		}
	}

	initElements(container) {
		this.mainOptions.beforeInitFunction(container);

		/*
			Init form validation
		*/
		$('form.needs-validation', container).jbobInitUninited().on('submit', (e) => {
			let form = $(e.currentTarget);

			if (this.formValidation(form) == false) {
				e.preventDefault();
				e.stopPropagation();
			}
		});

		/*
			Init async modals
		*/
		this.assignIDs(['.async-modal'], container);

		$('.async-modal', container).jbobInitUninited().click((e) => {
			e.preventDefault();
			let node = $(e.currentTarget);
			this.loadAsyncModal(node);
		});

		/*
			Init async accordions
		*/

		this.assignIDs(['.async-accordion'], container);

		$('.async-accordion > .accordion-collapse', container).jbobInitUninited().on('show.bs.collapse', (e) => {
			e.stopPropagation();
			let node = $(e.currentTarget).closest('.async-accordion');
			this.loadAsyncAccordion(node, false);
		});

		/*
			Init async tabs
		*/

		this.assignIDs(['.async-tab'], container);

		$('.async-tab', container).jbobInitUninited().on('show.bs.tab', (e) => {
			this.loadAsyncTab($(e.currentTarget), false);
		}).on('hidden.bs.tab', (e) => {
			e.stopPropagation();

			let node = $(e.currentTarget);
			if (node.hasClass('keep-contents') == false) {
				let pane_id = node.attr('data-bs-target');
				$(pane_id).empty();
			}
		});

		/*
			Init async popovers
		*/

		$('.async-popover', container).jbobInitUninited().one('show.bs.popover', (e) => {
			let pop = $(e.currentTarget);
			pop.trigger('jb-before-async-fetch');

			let url = pop.attr('data-contents-url');
			$.ajax({
				url: url,
				method: 'GET',
				dataType: 'HTML',

				success: (data) => {
					pop.attr('data-bs-content', data);
					pop.popover('dispose').popover('show');
					pop.trigger('jb-after-async-fetch', [true]);
				},
				error: () => {
					pop.trigger('jb-after-async-fetch', [false]);
				}
			});
		});

		/*
			Init dynamic tables
		*/

		$('.dynamic-table', container).jbobInitUninited().each(function() {
			$(this).find('> tfoot').find('input, select, textarea').addClass('skip-on-submit');
		}).on('click', '.add-row', (e) => {
			e.preventDefault();
			let btn = $(e.currentTarget);
			let table = btn.closest('.dynamic-table');
			let row = table.find('> tfoot tr').first().clone(true, true);
			row.find('.skip-on-submit').removeClass('skip-on-submit');
			btn.closest('tr').before(row);
			this.initElements(row);
			table.trigger('jb-table-row-added', [row]);
		}).on('click', '.remove-row', function(e) {
			e.preventDefault();
			let row = $(this).closest('tr');
			let table = row.closest('.dynamic-table');
			table.trigger('jb-table-row-removing', [table, row]);
			row.remove();
			table.trigger('jb-table-row-removed', [table, row]);
		});

		/*
			Init modal forms
		*/

		$('.modal-form', container).jbobInitUninited().on('submit', (e) => {
			e.preventDefault();
			let form = $(e.currentTarget);
			if (this.formValidation(form)) {
				let params = this.formAjaxParams(form);
				params.success = () => {
					form.closest('.modal').modal('hide');
				};

				$.ajax(params);
			}
		});

		/*
			Init dynamic forms
		*/

		$('.dynamic-form', container).jbobInitUninited().each((index, node) => {
			let form = $(node);

			let private_names = [
				'jb-close-modal',
				'jb-close-all-modals',
				'jb-post-saved-function',
				'jb-reload-page',
			];

			form.find('input[type=hidden]').each((index, hid) => {
				let hidden = $(hid);
				let name = hidden.attr('name');
				if (private_names.includes(name)) {
					hidden.addClass('skip-on-submit');
				}
			});
		}).on('submit', (e) => {
			e.preventDefault();
			let form = $(e.currentTarget);
			if (this.formValidation(form)) {
				let params = this.formAjaxParams(form);
				params.success = (data) => {
					form.find('input[type=hidden]').each((index, node) => {
						let hidden = $(node);
						let name = hidden.attr('name');

						switch (name) {
							case 'jb-close-modal':
								form.closest('.modal').modal('hide');
								break;

							case 'jb-close-all-modals':
								$('.modal.fade.show').modal('hide');
								break;

							case 'jb-post-saved-function':
								let function_name = hidden.attr('value');
								let fn = this.mainOptions.dynamicFunctions[function_name];
								if (typeof fn === 'function') {
									fn(form, data);
								}
								break;

							case 'jb-reload-page':
								location.reload();
								break;
						}
					});
				};

				$.ajax(params);
			}
		});

		/*
			Init filterable panel
		*/

		$('.filterable-panel', container).jbobInitUninited().each((index, node) => {
			$(node).find('input[type=search]').keyup((e) => {
				let box = $(e.currentTarget);
				let panel = box.closest('.filterable-panel');
				panel.trigger('jb-before-async-fetch');

				let data = {};
				let form = box.closest('form');
				if (form.length) {
					data = this.serializeForm(form);
				} else {
					let name = box.attr('name');
					data[name] = box.val();
				}

				$.ajax({
					url: panel.attr('data-endpoint'),
					data: data,
					dataType: 'HTML',
					success: (data) => {
						let d = $(data).find('.filterable-block').html();

						if (d) {
							d = $(d);
							this.initElements(d);
							panel.find('.filterable-block').empty().append(d);
						}

						panel.trigger('jb-after-async-fetch', [true]);
					},
					error: () => {
						panel.trigger('jb-after-async-fetch', [false]);
					}
				});
			});
		});

		/*
			Init infinite scroll
		*/

		$('.infinite-scroll', container).jbobInitUninited().each((index, node) => {
			let scrollable = $(node);
			let paginator = scrollable.find('.pagination').closest('nav');
			let loading = false;

			$(window).on('scroll', () => {
				if (this.onScreen(paginator, 200) && loading == false) {
					loading = true;
					let next = paginator.find('li.active').next('li');
					paginator.remove();

					if (next.length > 0) {
						scrollable.trigger('jb-before-async-fetch');

						$.ajax({
							url: next.find('a').attr('href'),
							method: 'GET',
							dataType: 'HTML',
							success: (data) => {
								data = $(data);
								let d = null;

								if (data.hasClass('infinite-scroll')) {
									d = data.html();
								} else {
									d = data.find('.infinite-scroll').html();
								}

								if (d) {
									d = $(d);
									this.initElements(d);
									scrollable.append(d);
									paginator = scrollable.find('.pagination').closest('nav');
								}

								loading = false;
								scrollable.trigger('jb-after-async-fetch', [true]);
							},
							error: () => {
								scrollable.trigger('jb-after-async-fetch', [false]);
							}
						});
					}
				}
			});
		});

		/*
			Dynamic pagination
		*/

		$('.dynamic-pagination', container).jbobInitUninited().each((index, node) => {
			let pagination = $(node);

			pagination.on('click', '.page-link', (e) => {
				let panel = pagination.closest('.pagination-block');
				if (panel.length == 0) {
					return;
				}

				e.preventDefault();
				let link = $(e.currentTarget);

				pagination.trigger('jb-before-async-fetch');

				$.ajax({
					url: link.attr('href'),
					method: 'GET',
					dataType: 'HTML',
					success: (data) => {
						data = $(data);
						let d = null;

						if (data.hasClass('pagination-block')) {
							d = data.html();
						} else {
							d = data.find('.pagination-block').html();
						}

						if (d) {
							d = $(d);
							this.initElements(d);
							panel.empty().append(d);
						}

						pagination.trigger('jb-after-async-fetch', [true]);
					},
					error: () => {
						pagination.trigger('jb-after-async-fetch', [false]);
					}
				});
			})
		});

		this.mainOptions.initFunction(container);
	}

	testActuallyAsyncLoad(node, force) {
		if (force) {
			return true;
		}

		if (node.hasClass('keep-contents') && node.hasClass('async-content-loaded')) {
			return false;
		}

		return true;
	}

	loadAsyncAccordion(node, force) {
		if (this.testActuallyAsyncLoad(node, force)) {
			node.trigger('jb-before-async-fetch');
			let url = node.attr('data-accordion-url');
			let body = node.find('.accordion-body').first();

			this.fetchNode(url, body)
				.then((success) => {
					if (success) {
						node.addClass('async-content-loaded');
					}

					node.trigger('jb-after-async-fetch', [success]);
				})
				.catch(() => {
					node.trigger('jb-after-async-fetch', [false]);
				});
		}
	}

	loadAsyncModal(node) {
		node.prop('disabled', true).trigger('jb-before-async-fetch');
		let url = node.attr('data-modal-url') || node.attr('href');

		this.fetchRemoteModal(url)
			.then((success) => {
				node.prop('disabled', false).trigger('jb-after-async-fetch', [success]);
			})
			.catch(() => {
				node.prop('disabled', false).trigger('jb-after-async-fetch', [false]);
			});
	}

	loadAsyncTab(node, force) {
		if (this.testActuallyAsyncLoad(node, force)) {
			node.trigger('jb-before-async-fetch');
			let pane_id = node.attr('data-bs-target');
			let pane = $(pane_id);
			let url = node.attr('data-tab-url');

			this.fetchNode(url, pane)
				.then((success) => {
					if (success) {
						node.addClass('async-content-loaded');
					}

					node.trigger('jb-after-async-fetch', [success]);
				})
				.catch(() => {
					node.trigger('jb-after-async-fetch', [false]);
				});
		}
	}

	async fetchRemoteModal(url) {
		return new Promise((resolve) => {
			$.ajax({
				url: url,
				method: 'GET',
				dataType: 'HTML',
				success: (data) => {
					try {
						let d = $(data);
						this.initElements(d);
						d.addClass('delete-on-close').modal('show');
						resolve(true);
					} catch (error) {
						resolve(false);
					}
				},
				error: () => {
					resolve(false);
				}
			});
		});
	}

	async fetchNode(url, body) {
		body.empty().append(this.makeSpinner());

		return new Promise((resolve) => {
			$.ajax({
				url: url,
				method: 'GET',
				dataType: 'HTML',
				success: (data) => {
					try {
						let d = $(data);
						this.initElements(d);
						body.empty().append(d);
						resolve(true);
					} catch (error) {
						resolve(false);
					}
				},
				error: () => {
					resolve(false);
				}
			});
		});
	}

	reloadNode(node) {
		let url = node.attr('data-reload-url');
		if (url) {
			if (node.hasClass('modal')) {
				this.fetchRemoteModal(url);
				node.modal('hide');
			} else {
				$.ajax({
					url: url,
					method: 'GET',
					dataType: 'HTML',
					success: (data) => {
						let d = $(data);
						this.initElements(d);
						node.replaceWith(d);
					}
				});
			}
		} else {
			node.reloadAsync();
		}
	}

	submitButton(form) {
		let ret = form.find('button[type=submit]');

		if (ret.length == 0) {
			let id = form.attr('id');
			if (id) {
				ret = $('button[type=submit][form=' + id + ']')
			}
		}

		return ret;
	}

	/*
		Internally used to populate the params for $.ajax() according to the
		form's enctype, and properly use serializeForm() or serializeFormData()
	*/
	formAjaxParams(form) {
		let ret = {
			method: form.attr('method'),
			url: form.attr('action'),
		};

		let enctype = form.attr('enctype');
		if (enctype == 'multipart/form-data') {
			ret.processData = false;
			ret.contentType = false;
			ret.data = this.serializeFormData(form);
		} else {
			ret.data = this.serializeForm(form);
		}

		return ret;
	}

	/*
		Wrapper around $.serialize() which skips the field marked to be not
		submitted
	*/
	serializeForm(form) {
		return form.find(':not(.skip-on-submit)').serialize();
	}

	/*
		Like serializeForm(), but produces a FormData (suitable for files
		uploads)
	*/
	serializeFormData(form) {
		let ret = new FormData();

		$.each(form.find('input[type="file"]:not(.skip-on-submit)'), function(i, tag) {
			$.each($(tag)[0].files, function(i, file) {
				ret.append(tag.name, file);
			});
		});

		let values = form.find(':not(.skip-on-submit)').serializeArray();
		$.each(values, function(i, val) {
			ret.append(val.name, val.value);
		});

		return ret;
	}

	onScreen(node, offset) {
		if (offset === undefined) {
			offset = 0;
		}

		let vtop = $(window).scrollTop();
		let vbottom = vtop + $(window).height();
		let top = node.offset().top;
		let bottom = top + node.outerHeight();
		return ((bottom <= vbottom + offset) && (top >= vtop - offset));
	}

	onceInit() {
		let jbob = this;

		$.fn.extend({
			jbobInitUninited: function() {
				return this.not('.jb-in').addClass('jb-in');
			},

			reloadAsync: function() {
				this.each((index, node) => {
					/*
						Here I try to guess if the target node is involved in a
						async-* function, which one, and act as expected for
						each case
					*/

					node = $(node);

					if (node.hasClass('async-modal')) {
						jbob.loadAsyncModal(node);
					} else if (node.hasClass('async-accordion')) {
						jbob.loadAsyncAccordion(node, true);
					} else if (node.hasClass('async-tab')) {
						jbob.loadAsyncTab(node, true);
					}
				});
			},
		});

		$('body').on('hidden.bs.modal', '.modal.delete-on-close', function() {
			$(this).remove()
		});
	}
}

export default jBob;
