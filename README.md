jBob
====

Opinionated utilities for [jQuery](https://jquery.com/) and [Bootstrap](https://getbootstrap.com/).

Includes:

- methods to dynamically fetch modals, tabs, accordions, pages and popovers
- infinite scroll based on Bootstrap's pagination
- dynamic tables to add and remove rows
- some spare JS utility

For full documentation, details and demos, visit the page [https://jbob.madbob.org/](https://jbob.madbob.org/)
